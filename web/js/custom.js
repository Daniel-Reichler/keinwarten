// preloader
$(window).load(function(){
    $('.preloader').fadeOut(1000); // set duration in brackets    
});

$(function() {
    new WOW().init();
    $('.templatemo-nav').singlePageNav({
    	offset: 70
    });

    /* Hide mobile menu after clicking on a link
    -----------------------------------------------*/
    $('.navbar-collapse a').click(function(){
        $(".navbar-collapse").collapse('hide');
    });
});

var selectService = function(buttonId, selectorId, redirect) {
    var link = $('#' + selectorId).val();
    $('#' + buttonId).attr('href', '/service/' + link);
    $('#' + buttonId).attr('disabled', false);
    if(redirect) {
        window.location.href = '/service/' + link;
    }
};

var packageSubmit = function() {
    var checkBoxes = function () {
        if(document.getElementById('agb').checked && document.getElementById('widerruf').checked) {
            return true;
        } else {
            return false;
        }
    };

    var personalData = function() {
        var fields = ["salutation", "firstname", "lastname", "street", "streetNr", "zip", "city", "phone", "email"];
        var error = false;

        $.each(fields, function(i, element){
            if ($('#' + element).val() === '') {
                error = true;
            }
        });

        return !error;
    };

    if(checkBoxes() && personalData()) {
        $('#submitBtnPackage').attr('disabled', false);
    } else {
        $('#submitBtnPackage').attr('disabled', true);
    }
};