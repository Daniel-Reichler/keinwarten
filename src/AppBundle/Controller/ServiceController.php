<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Service;
use AppBundle\Entity\ServiceInfo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/service")
 */
class ServiceController extends Controller
{
    /**
     * @Route("/{serviceUrl}")
     */
    public function showServiceInfoAction($serviceUrl)
    {
        /** @var Service $service */
        if (!$service = $this->getDoctrine()
            ->getRepository('AppBundle:Service')
            ->findOneBy(array('urlName' => $serviceUrl))
        ) {
            throw $this->createNotFoundException('Der Service wurde nicht gefunden!');
        }
        /** @var ServiceInfo $serviceInfo */
        if (!$serviceInfo = $this->getDoctrine()
            ->getRepository('AppBundle:ServiceInfo')
            ->findOneBy(array('serviceId' => $service->getId()))
        ) {
            throw $this->createNotFoundException('Die Servicedetails wurden nicht gefunden!');
        }

        return $this->render('AppBundle:Service:show_service_info.html.twig', array(
            'service' => $service,
            'lowestPrice' => $service->getPackages()[0]->getPrice(),
            'feesIncluded' => $service->getPackages()[0]->getFeesIncluded(),
            'fees' => $service->getPackages()[0]->getFees(),
            'info' => $serviceInfo
        ));
    }

}
