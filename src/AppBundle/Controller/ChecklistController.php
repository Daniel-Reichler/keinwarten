<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Checklist;
use AppBundle\Entity\ChecklistItem;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class ChecklistController
 * @package AppBundle\Controller
 * @Route("/checklisten")
 */
class ChecklistController extends Controller
{
    /**
     * @Route("/")
     */
    public function showChecklistsOverviewAction()
    {
        $checklists = $this->getDoctrine()
            ->getRepository('AppBundle:Checklist')
            ->findAll();

        return $this->render('@App/Checklist/show_checklists.html.twig', array(
            'checklists' => $checklists
        ));
    }

    /**
     * @Route("/{serviceUrl}", name="checklist_service")
     */
    public function showChecklistAction($serviceUrl)
    {
        /** @var Service $service */
        if (!$service = $this->getDoctrine()
            ->getRepository('AppBundle:Service')
            ->findOneBy(array('urlName' => $serviceUrl))
        ) {
            throw $this->createNotFoundException('Der Service wurde nicht gefunden!');
        }
        /** @var Checklist $checklist */
        if (!$checklists = $this->getDoctrine()
            ->getRepository('AppBundle:Checklist')
            ->findBy(array('serviceId' => $service->getId()))
        ) {
            throw $this->createNotFoundException('Die Checkliste wurden nicht gefunden!');
        }

        return $this->render('@App/Checklist/show_checklist.html.twig', array(
            'service' => $service,
            'checklists' => $checklists,
        ));
    }

}
