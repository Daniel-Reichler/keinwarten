<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Contact;
use AppBundle\Entity\Job;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use AppBundle\Entity\Date;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;

class ShopController extends Controller
{
    /**
     * @Route("/warenkorb/{service}")
     */
    public function serviceIndexAction($service)
    {
        if (!$service = $this->getDoctrine()
            ->getRepository('AppBundle:Service')
            ->findOneBy(array('urlName' => $service))
        ) {
            throw $this->createNotFoundException('Der Service wurde nicht gefunden!');
        }

        return $this->render('AppBundle:Shop:warenkorb.html.twig', array(
            'service' => $service
        ));
    }

    /**
     * @Route("/warenkorb/{service}/{package}", name="service_package")
     */
    public function servicePackageAction($service, $package, Request $request)
    {
        $error = false;
        if ($request->get('error') !== null || $request === true || $request === 1) {
            $error = true;
        }

        if (!$service = $this->getDoctrine()
            ->getRepository('AppBundle:Service')
            ->findOneBy(array('urlName' => $service))
        ) {
            throw $this->createNotFoundException('Der Service wurde nicht gefunden!');
        }

        if (!$package = $this->getDoctrine()
            ->getRepository('AppBundle:ServicePackage')
            ->findOneBy(array('serviceId' => $service->getId(), 'urlName' => $package))
        ) {
            throw $this->createNotFoundException('Das Service-Paket wurde nicht gefunden!');
        }

        $dates = [];
        $dateMin =  new \DateTime();
        $dateMin->modify('+36 hours');
        $dateMax = new \DateTime();
        $dateMax->modify('+10 days');

        $dateRepository = $this->getDoctrine()
            ->getRepository(Date::class);

        $dates = $dateRepository
            ->createQueryBuilder('d')
            ->where('d.date <= :dateMax')
            ->andWhere('d.date >= :dateMin')
            ->andWhere('d.available = true')
            ->setParameters(array(':dateMax' => $dateMax, ':dateMin' => $dateMin))
            ->getQuery()
            ->execute();

        $availableDates = [];
        foreach ($dates as $date) {

            $ar = [
                'time' => $date->getDate()->format('H:i'),
                'id' => $date->getId()
            ];
            if (!isset($availableDates[$date->getDate()->format('d.m.Y')])) {
                $availableDates[$date->getDate()->format('d.m.Y')] = [];
            }
            array_push($availableDates[$date->getDate()->format('d.m.Y')], $ar);
        }

        return $this->render('AppBundle:Shop:package.html.twig', array(
            'service' => $service,
            'package' => $package,
            'dates'     => $availableDates,
            'error' => $error
        ));
    }

    /**
     * @Route("/beauftragen/{service}/{package}")
     */
    public function checkoutAction(Request $request, $service, $package)
    {
        $serviceUrl = $service;
        $packageUrl = $package;
        $em = $this->getDoctrine()->getManager();

        if (!$service = $this->getDoctrine()
            ->getRepository('AppBundle:Service')
            ->findOneBy(array('urlName' => $service))
        ) {
            throw $this->createNotFoundException('Der Service wurde nicht gefunden!');
        }

        if (!$package = $this->getDoctrine()
            ->getRepository('AppBundle:ServicePackage')
            ->findOneBy(array('serviceId' => $service->getId(), 'urlName' => $package))
        ) {
            throw $this->createNotFoundException('Das Service-Paket wurde nicht gefunden!');
        }

        if ($request->get('date') == 'n')
        {
            $date = false;
        }
        else {
            if (!$date = $this->getDoctrine()
                ->getRepository('AppBundle:Date')
                ->find($request->get('date'))
            ) {
                throw $this->createNotFoundException('Der Termin wurde nicht gefunden!');
            }
        }

        $contact = new Contact();

        $contact->setTitle($request->get('title'));
        $contact->setSalutation($request->get('salutation'));
        $contact->setFirstname($request->get('firstname'));
        $contact->setLastname($request->get('lastname'));
        $contact->setStreet($request->get('street'));
        $contact->setStreetNr($request->get('streetNr'));
        $contact->setZip($request->get('zip'));
        $contact->setCity($request->get('city', 'Nürnberg'));
        $contact->setPhone($request->get('phone'));
        $contact->setEmail($request->get('email'));
        $contact->setType('delivery');

        $validator = $this->get('validator');
        $errors = $validator->validate($contact);

        $em->persist($contact);
        $em->flush();

        return $this->render('AppBundle:Shop:checkout.html.twig', array(
            'service' => $service,
            'package' => $package,
            'contact' => $contact,
            'date'     => $date,
        ));

    }

    /**
     * @Route("/beauftragen/{service}/{package}/{dateId}/{contactId}/{payment}")
     */
    public function checkoutCompleteAction(Request $request, $service, $package, $dateId, $contactId, $payment, \Swift_Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();

        if (!$service = $this->getDoctrine()
            ->getRepository('AppBundle:Service')
            ->findOneBy(array('urlName' => $service))
        ) {
            throw $this->createNotFoundException('Der Service wurde nicht gefunden!');
        }

        if (!$package = $this->getDoctrine()
            ->getRepository('AppBundle:ServicePackage')
            ->findOneBy(array('serviceId' => $service->getId(), 'urlName' => $package))
        ) {
            throw $this->createNotFoundException('Das Service-Paket wurde nicht gefunden!');
        }

        if (!$contact = $this->getDoctrine()
            ->getRepository('AppBundle:Contact')
            ->find($contactId)
        ) {
            throw $this->createNotFoundException('Der Termin wurde nicht gefunden!');
        }

        if ($dateId == 'n')
        {
            $date = null;
        }
        else {
            if (!$date = $this->getDoctrine()
                ->getRepository('AppBundle:Date')
                ->find($dateId)
            ) {
                throw $this->createNotFoundException('Der Termin wurde nicht gefunden!');
            }
            $dateId = $date->getId();
        }

        /*if (!$date->getAvailable()) {
            throw $this->createNotFoundException('Der Termin ist leider nicht mehr verfügbar!');
        }*/

        $job = new Job();

        $job->setContactId($contact->getId());
        $job->setDateId($dateId);
        $job->setPayment($payment);
        $job->setPrice($package->getPrice());
        $job->setCreatedAt(new \DateTime());
        $job->setServicePackageId($package->getId());

        if ($date !== null) {
            $date->setAvailable(false);
            $em->persist($date);
        }

        $em->persist($job);
        $em->flush();

        $message = (new \Swift_Message('Neue Bestellung'))
            ->setFrom('support@amtagenten.de')
            ->setTo('support@amtagenten.de')
            ->setBody(
                $this->renderView(
                // app/Resources/views/Emails/registration.html.twig
                    'AppBundle:emails:order.html.twig',
                    array(
                        'contact' => $contact,
                        'date' => $date,
                        'service' => $service,
                        'package' => $package,
                        'payment' => $payment
                    )
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'Emails/registration.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )
            */
        ;

        $mailer->send($message);
        return $this->redirectToRoute('order_finished', array('state' => 'success'));
    }


    /**
     * @Route("/beauftragen/danke", name="order_finished")
     */
    public function orderFinished() {
        return $this->render('AppBundle:Shop:completed.html.twig', array(
        ));

    }
}
