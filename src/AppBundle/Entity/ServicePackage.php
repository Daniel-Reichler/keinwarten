<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServicePackage
 *
 * @ORM\Table(name="service_package")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServicePackageRepository")
 */
class ServicePackage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="service_id", type="integer")
     */
    private $serviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="url_name", type="string", length=50)
     */
    private $urlName;

    /**
     * @var string
     *
     * @ORM\Column(name="display_level", type="string", length=10)
     */
    private $displayLevel;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="decimal", precision=7, scale=2)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @ORM\Column(name="features", type="simple_array")
     */
    private $features;

    /**
     * @ORM\Column(name="fees_included", type="boolean")
     */
    private $feesIncluded;

    /**
     * @ORM\Column(name="fees", type="decimal", precision=7, scale=2, nullable=true)
     */
    private $fees;

    /**
     * @ORM\ManyToOne(targetEntity="Service", inversedBy="packages")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;

    public function getFeatures()
    {
        return $this->features;
    }

    public function getDisplayLevel()
    {
        return $this->displayLevel;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set serviceId
     *
     * @param integer $serviceId
     *
     * @return ServicePackage
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;

        return $this;
    }

    /**
     * Get serviceId
     *
     * @return int
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return ServicePackage
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set urlName
     *
     * @param string $urlName
     *
     * @return ServicePackage
     */
    public function setUrlName($urlName)
    {
        $this->urlName = $urlName;

        return $this;
    }

    /**
     * Get urlName
     *
     * @return string
     */
    public function getUrlName()
    {
        return $this->urlName;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return ServicePackage
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ServicePackage
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get feesInclude
     */
    public function getFeesIncluded()
    {
       return $this->feesIncluded;
    }

    /**
     * Get feesInclude
     */
    public function getFees()
    {
        return $this->fees;
    }
}

