<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServiceInfo
 *
 * @ORM\Table(name="service_info")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServiceInfoRepository")
 */
class ServiceInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="service_id", type="integer")
     */
    private $serviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="step1Title", type="string", length=255)
     */
    private $step1Title;

    /**
     * @var string
     *
     * @ORM\Column(name="step1Text", type="text")
     */
    private $step1Text;

    /**
     * @var string
     *
     * @ORM\Column(name="step2Title", type="string", length=255)
     */
    private $step2Title;

    /**
     * @var string
     *
     * @ORM\Column(name="step2Text", type="text")
     */
    private $step2Text;

    /**
     * @var string
     *
     * @ORM\Column(name="step3Title", type="string", length=255)
     */
    private $step3Title;

    /**
     * @var string
     *
     * @ORM\Column(name="step3Text", type="text")
     */
    private $step3Text;

    /**
     * @var string
     *
     * @ORM\Column(name="stepsTitle", type="text")
     */
    private $stepsTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="detailsTitle", type="text")
     */
    private $detailsTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="detailsText", type="text")
     */
    private $detailsText;

    /**
     * @var string
     *
     * @ORM\Column(name="faq1Title", type="text", nullable=true)
     */
    private $faq1Title;

    /**
     * @var string
     *
     * @ORM\Column(name="faq1Text", type="text", nullable=true)
     */
    private $faq1Text;

    /**
     * @var string
     *
     * @ORM\Column(name="faq2Title", type="text", nullable=true)
     */
    private $faq2Title;

    /**
     * @var string
     *
     * @ORM\Column(name="faq2Text", type="text", nullable=true)
     */
    private $faq2Text;

    /**
     * @var string
     *
     * @ORM\Column(name="faq3Title", type="text", nullable=true)
     */
    private $faq3Title;

    /**
     * @var string
     *
     * @ORM\Column(name="faq3Text", type="text", nullable=true)
     */
    private $faq3Text;

    /**
     * @var string
     *
     * @ORM\Column(name="faq4Title", type="text", nullable=true)
     */
    private $faq4Title;

    /**
     * @var string
     *
     * @ORM\Column(name="faq4Text", type="text", nullable=true)
     */
    private $faq4Text;

    /**
     * @var string
     *
     * @ORM\Column(name="faq5Title", type="text", nullable=true)
     */
    private $faq5Title;

    /**
     * @var string
     *
     * @ORM\Column(name="faq5Text", type="text", nullable=true)
     */
    private $faq5Text;

    /**
     * @var string
     *
     * @ORM\Column(name="moreServicesTitle", type="text", nullable=true)
     */
    private $moreServicesTitle;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set step1Title
     *
     * @param string $step1Title
     *
     * @return ServiceInfo
     */
    public function setStep1Title($step1Title)
    {
        $this->step1Title = $step1Title;

        return $this;
    }

    /**
     * Get step1Title
     *
     * @return string
     */
    public function getStep1Title()
    {
        return $this->step1Title;
    }

    /**
     * Set step1Text
     *
     * @param string $step1Text
     *
     * @return ServiceInfo
     */
    public function setStep1Text($step1Text)
    {
        $this->step1Text = $step1Text;

        return $this;
    }

    /**
     * Get step1Text
     *
     * @return string
     */
    public function getStep1Text()
    {
        return $this->step1Text;
    }

    /**
     * Set step2Title
     *
     * @param string $step2Title
     *
     * @return ServiceInfo
     */
    public function setStep2Title($step2Title)
    {
        $this->step2Title = $step2Title;

        return $this;
    }

    /**
     * Get step2Title
     *
     * @return string
     */
    public function getStep2Title()
    {
        return $this->step2Title;
    }

    /**
     * Set step2Text
     *
     * @param string $step2Text
     *
     * @return ServiceInfo
     */
    public function setStep2Text($step2Text)
    {
        $this->step2Text = $step2Text;

        return $this;
    }

    /**
     * Get step2Text
     *
     * @return string
     */
    public function getStep2Text()
    {
        return $this->step2Text;
    }

    /**
     * Set step3Title
     *
     * @param string $step3Title
     *
     * @return ServiceInfo
     */
    public function setStep3Title($step3Title)
    {
        $this->step3Title = $step3Title;

        return $this;
    }

    /**
     * Get step3Title
     *
     * @return string
     */
    public function getStep3Title()
    {
        return $this->step3Title;
    }

    /**
     * Set step3Text
     *
     * @param string $step3Text
     *
     * @return ServiceInfo
     */
    public function setStep3Text($step3Text)
    {
        $this->step3Text = $step3Text;

        return $this;
    }

    /**
     * Get step3Text
     *
     * @return string
     */
    public function getStep3Text()
    {
        return $this->step3Text;
    }

    /**
     * Set stepsTitle
     *
     * @param string $stepsTitle
     *
     * @return ServiceInfo
     */
    public function setStepsTitle($stepsTitle)
    {
        $this->stepsTitle = $stepsTitle;

        return $this;
    }

    /**
     * Get stepsTitle
     *
     * @return string
     */
    public function getStepsTitle()
    {
        return $this->stepsTitle;
    }

    /**
     * Set detailsTitle
     *
     * @param string $detailsTitle
     *
     * @return ServiceInfo
     */
    public function setDetailsTitle($detailsTitle)
    {
        $this->detailsTitle = $detailsTitle;

        return $this;
    }

    /**
     * Get detailsTitle
     *
     * @return string
     */
    public function getDetailsTitle()
    {
        return $this->detailsTitle;
    }

    /**
     * Set detailsText
     *
     * @param string $detailsText
     *
     * @return ServiceInfo
     */
    public function setDetailsText($detailsText)
    {
        $this->detailsText = $detailsText;

        return $this;
    }

    /**
     * Get detailsText
     *
     * @return string
     */
    public function getDetailsText()
    {
        return $this->detailsText;
    }

    /**
     * Set faq1Title
     *
     * @param string $faq1Title
     *
     * @return ServiceInfo
     */
    public function setFaq1Title($faq1Title)
    {
        $this->faq1Title = $faq1Title;

        return $this;
    }

    /**
     * Get faq1Title
     *
     * @return string
     */
    public function getFaq1Title()
    {
        return $this->faq1Title;
    }

    /**
     * Set faq1Text
     *
     * @param string $faq1Text
     *
     * @return ServiceInfo
     */
    public function setFaq1Text($faq1Text)
    {
        $this->faq1Text = $faq1Text;

        return $this;
    }

    /**
     * Get faq1Text
     *
     * @return string
     */
    public function getFaq1Text()
    {
        return $this->faq1Text;
    }

    /**
     * Set faq2Title
     *
     * @param string $faq2Title
     *
     * @return ServiceInfo
     */
    public function setFaq2Title($faq2Title)
    {
        $this->faq2Title = $faq2Title;

        return $this;
    }

    /**
     * Get faq2Title
     *
     * @return string
     */
    public function getFaq2Title()
    {
        return $this->faq2Title;
    }

    /**
     * Set faq2Text
     *
     * @param string $faq2Text
     *
     * @return ServiceInfo
     */
    public function setFaq2Text($faq2Text)
    {
        $this->faq2Text = $faq2Text;

        return $this;
    }

    /**
     * Get faq2Text
     *
     * @return string
     */
    public function getFaq2Text()
    {
        return $this->faq2Text;
    }

    /**
     * Set faq3Title
     *
     * @param string $faq3Title
     *
     * @return ServiceInfo
     */
    public function setFaq3Title($faq3Title)
    {
        $this->faq3Title = $faq3Title;

        return $this;
    }

    /**
     * Get faq3Title
     *
     * @return string
     */
    public function getFaq3Title()
    {
        return $this->faq3Title;
    }

    /**
     * Set faq3Text
     *
     * @param string $faq3Text
     *
     * @return ServiceInfo
     */
    public function setFaq3Text($faq3Text)
    {
        $this->faq3Text = $faq3Text;

        return $this;
    }

    /**
     * Get faq3Text
     *
     * @return string
     */
    public function getFaq3Text()
    {
        return $this->faq3Text;
    }

    /**
     * Set faq4Title
     *
     * @param string $faq4Title
     *
     * @return ServiceInfo
     */
    public function setFaq4Title($faq4Title)
    {
        $this->faq4Title = $faq4Title;

        return $this;
    }

    /**
     * Get faq4Title
     *
     * @return string
     */
    public function getFaq4Title()
    {
        return $this->faq4Title;
    }

    /**
     * Set faq4Text
     *
     * @param string $faq4Text
     *
     * @return ServiceInfo
     */
    public function setFaq4Text($faq4Text)
    {
        $this->faq4Text = $faq4Text;

        return $this;
    }

    /**
     * Get faq4Text
     *
     * @return string
     */
    public function getFaq4Text()
    {
        return $this->faq4Text;
    }

    /**
     * Set faq5Title
     *
     * @param string $faq5Title
     *
     * @return ServiceInfo
     */
    public function setFaq5Title($faq5Title)
    {
        $this->faq5Title = $faq5Title;

        return $this;
    }

    /**
     * Get faq5Title
     *
     * @return string
     */
    public function getFaq5Title()
    {
        return $this->faq5Title;
    }

    /**
     * Set faq5Text
     *
     * @param string $faq5Text
     *
     * @return ServiceInfo
     */
    public function setFaq5Text($faq5Text)
    {
        $this->faq5Text = $faq5Text;

        return $this;
    }

    /**
     * Get faq5Text
     *
     * @return string
     */
    public function getFaq5Text()
    {
        return $this->faq5Text;
    }

    /**
     * Set moreServicesTitle
     *
     * @param string $moreServicesTitle
     *
     * @return ServiceInfo
     */
    public function setMoreServicesTitle($moreServicesTitle)
    {
        $this->moreServicesTitle = $moreServicesTitle;

        return $this;
    }

    /**
     * Get moreServicesTitle
     *
     * @return string
     */
    public function getMoreServicesTitle()
    {
        return $this->moreServicesTitle;
    }
}

